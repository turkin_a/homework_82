const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Album = require('../models/Album');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});
const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    if (req.query.artist) {
      Album.find({artist: req.query.artist})
        .then(results => res.send(results))
        .catch(() => res.status(404).send({error: 'Artist is not found'}));
    }
    else Album.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.get('/:id', (req, res) => {
    Album.findById(req.params.id).populate('artist')
      .then(results => res.send(results))
      .catch(() => res.status(404).send({error: 'Album is not found'}));
  });

  router.post('/', upload.single('poster'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
      albumData.poster = req.file.filename;
    } else {
      albumData.poster = null;
    }

    const album = new Album(albumData);

    album.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;