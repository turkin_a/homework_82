const express = require('express');

const Track = require('../models/Track');
const Album = require('../models/Album');

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    if (req.query.album) {
      Track.find({album: req.query.album})
        .then(results => res.send(results))
        .catch(() => res.status(404).send({error: 'Album is not found'}));
    }
    else if (req.query.artist) {
      Album.find({artist: req.query.artist})
        .then(results => {
          console.log(results);
        })
        .catch(() => res.status(404).send({error: 'Artist is not found'}));

      // Album.find({artist: req.query.artist})
      //   .then(results => {
      //     res.send(Promise.all(results.map(album => {
      //       Track.find({album: album._id})
      //         .then(results => {
      //           return results;
      //         })
      //         .catch(() => res.status(404).send({error: 'Album is not found'}));
      //     })))
      //   })
      //   .catch(() => res.status(404).send({error: 'Artist is not found'}));
    }
    else Track.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', (req, res) => {
    const track = new Track(req.body);

    track.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;